package StaticServer

import java.io.File


class Response(request: Request, private val config: Config) {
    val headers: String
    val status: Response.Status
    val contentType: Response.ContentType
    val path: String

    init {
        val index = request.path.last() == '/'

        path = "${config.documentRoot}${request.path}${if (index) "index.html" else ""}"

        status = if (!request.valid) {
            Status.NOT_ALLOWED
        } else if (!validatePath(request.path) || (index && !File(path).exists())) {
            Status.FORBIDDEN
        } else if (!File(path).isFile) {
            Status.NOT_FOUND
        } else {
            Status.OK
        }

        contentType = ContentType.fromFileExt(request.path.fileExtension())

        val okHeaders = if (status == Status.OK) {
            """Content-Length: ${File(path).length()}
              |Content-Type: $contentType
              |
            """.trimMargin()
        } else{
            ""
        }

        headers = """HTTP/1.1 $status
                    |Server: localhost
                    |Date: ${java.time.LocalDateTime.now()}
                    |Connection: close
                    |$okHeaders
                    |
                  """.trimMargin().replace("\n", "\r\n")
    }

    companion object {
        fun validatePath(path: String): Boolean {
            return !path.contains("../")
        }
    }

    enum class Status(val code: Int, val text: String) {
        OK(200, "OK"),
        FORBIDDEN(403, "Forbidden"),
        NOT_FOUND(404, "Not Found"),
        NOT_ALLOWED(405, "Method Not Allowed");

        override fun toString(): String {
            return "$code $text"
        }
    }

    enum class ContentType(val mimeType: String) {
        HTML("text/html"),
        CSS("text/css"),
        TXT("text/plain"),
        JS("application/javascript"),
        JPG("image/jpeg"),
        JPEG("image/jpeg"),
        PNG("image/png"),
        GIF("image/gif"),
        SWF("application/x-shockwave-flash"),
        UNKNOWN("");

        override fun toString(): String {
            return this.mimeType
        }

        companion object {
            fun fromFileExt(fileExt: String): ContentType = when(fileExt.toLowerCase()) {
                "html" -> HTML
                "css" -> CSS
                "txt" -> TXT
                "js" -> JS
                "jpg" -> JPG
                "jpeg" -> JPEG
                "png" -> PNG
                "gif" -> GIF
                "swf" -> SWF
                else -> UNKNOWN
            }
        }
    }
}
