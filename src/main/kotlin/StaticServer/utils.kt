package StaticServer


fun String.fileExtension(): String {
    return this.substringAfterLast(".")
}
