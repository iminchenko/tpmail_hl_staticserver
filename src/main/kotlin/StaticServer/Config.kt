package StaticServer

import java.io.File

class Config(filename: String) {
    var cpuLimit: Int = 4
        private set
    var threadLimit: Int = 256
        private set
    var documentRoot: String = "."
        private set
    val port = 80
    init {
        File(filename).bufferedReader().use {
            while (it.ready()) {
                val temp = it.readLine().split(" ")

                if (temp.size >= 2) {
                    when (temp[0]) {
                        "cpu_limit" -> cpuLimit = temp[1].toInt()
                        "thread_limit" -> threadLimit = temp[1].toInt()
                        "document_root" -> documentRoot = temp[1]
                    }
                }
            }
        }
    }
}