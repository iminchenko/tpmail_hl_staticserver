package StaticServer

import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.net.Socket


class ClientHandler(private val client: Socket, private val config: Config) {
    fun handle() {
        try {
            val reader = BufferedReader(InputStreamReader(client.inputStream))
            val writer = BufferedOutputStream(client.getOutputStream())

            val request = Request(reader.readLine()!!)

            val response = Response(request, config)

            writer.write(response.headers.toByteArray(Charsets.UTF_8))

//            println(response.path)
//            println(response.headers)

            if (request.method == Request.Method.GET && response.status == Response.Status.OK) {
                File(response.path).bufferedReader().use {
                    while (it.ready()) {
                        writer.write(it.readLine().toByteArray(Charsets.UTF_8))
                        writer.write("\n".toByteArray(Charsets.UTF_8))
                    }
                }
            }

            writer.flush()
        }
        catch (ex: Exception) {
            println("[ERROR] ${ex.message}")
        }

        client.close()
//        println("[Connection closed]")
    }
}
