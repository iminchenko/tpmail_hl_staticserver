package StaticServer

import java.net.URLDecoder


class Request(text: String) {
    val valid: Boolean
    val method: Request.Method
    val path: String

    init {
        val splitted = text.split(" ")

        method = Method.fromString(splitted[0])

        path = if (splitted.size > 1) URLDecoder.decode(splitted[1], "utf-8").substringBefore("?") else "/"

        valid = method != Method.NOT_VALID
    }

    enum class Method {
        GET,
        HEAD,
        NOT_VALID;

        companion object {
            fun fromString(method: String?): Method = when(method?.toUpperCase()) {
                "GET" -> GET
                "HEAD" -> HEAD
                else -> NOT_VALID
            }
        }
    }
}
