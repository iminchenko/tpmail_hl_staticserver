package StaticServer

import java.net.ServerSocket
import kotlinx.coroutines.experimental.*


fun main(args: Array<String>) = runBlocking {
    val config = Config(".config")

    val server = ServerSocket(config.port)

    val context = newFixedThreadPoolContext(config.cpuLimit, "tpmail_poolcontext")

    println("Server started at port ${server.localPort}")

    while (true) {
        val client = server.accept()

//        println("[Client accepted]")

        GlobalScope.launch(context) {
            ClientHandler(client, config).handle()
        }
    }
}
