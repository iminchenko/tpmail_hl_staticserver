FROM ubuntu:16.04

RUN apt-get -y update

RUN apt-get install -y openjdk-8-jdk-headless tree

ENV WORK /tpmail_hl
WORKDIR $WORK/

ADD . .
RUN ./gradlew build
EXPOSE 80

CMD java -jar build/libs/tpmail_hl-fat-1.0-SNAPSHOT.jar